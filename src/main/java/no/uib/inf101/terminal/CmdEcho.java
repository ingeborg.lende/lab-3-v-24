package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    @Override
    public String run(String[] args) {
        StringBuilder result = new StringBuilder();

        for (String arg : args) {
            result.append(arg).append(" ");
        }

        return result.toString();
    }

    @Override
    public String getName() {
        return "echo";
    }
}
